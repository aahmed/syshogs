#!/bin/bash
# Prepared by Arif Ahmed (arifch2009@gmail.com)

MEM_USAGE="Time  Memory"

start_time=$(date +%T)

function finish() {
    end_time=$(date +%T)
    output_file=$start_time-$end_time-"mem.csv"
    echo -e $MEM_USAGE >> $output_file 

    exit 
}

trap finish SIGINT

while true; do
  #MB units
  mem_used=`free -m | grep "Mem" | awk '{print $3}'`

  MEM_USAGE+="\n$(date +%T)  $mem_used" 
     
  sleep 1
  
done
