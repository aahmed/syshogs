#!/bin/bash
# Prepared by Arif Ahmed (arifch2009@gmail.com)

PREV_TOTAL=0
PREV_IDLE=0

CPU_USAGE="Time  CPU(%)"

start_time=$(date +%T)

function finish() {
    end_time=$(date +%T)
    output_file=$start_time-$end_time-"cpu.csv"
    echo -e $CPU_USAGE >> $output_file 
    exit 
}

trap finish SIGINT

while true; do

  CPU=(`cat /proc/stat | grep '^cpu '`) # Get the total CPU statistics.
  unset CPU[0]                          # Discard the "cpu" prefix.
  IDLE=${CPU[4]}                        # Get the idle CPU time.

  # Calculate the total CPU time.
  TOTAL=0

  for VALUE in "${CPU[@]:0:4}"; do
    let "TOTAL=$TOTAL+$VALUE"
  done

  # Calculate the CPU usage since we last checked.
  let "DIFF_IDLE=$IDLE-$PREV_IDLE"
  let "DIFF_TOTAL=$TOTAL-$PREV_TOTAL"
  let "DIFF_USAGE=(1000*($DIFF_TOTAL-$DIFF_IDLE)/$DIFF_TOTAL+5)/10"
  
  #echo -en "CPU: $DIFF_USAGE%   "  
  #date +"%T" 

  CPU_USAGE+="\n$(date +%T)  $DIFF_USAGE" 

  
  # Remember the total and idle CPU times for the next check.
  PREV_TOTAL="$TOTAL"
  PREV_IDLE="$IDLE"
  # Wait before checking again.
    
  sleep 1
  
done
