#!/bin/bash

# Default interface 
IF="$(ip route get 8.8.8.8 | awk -- '{printf $5}')" # Automatically select interface 


if [ -z "$IF" ]; then
        IF=`ls -1 /sys/class/net/ | head -1`
fi
RXPREV=-1
TXPREV=-1


NET_USAGE="Time  Received(B/s)  Send(B/s)"

start_time=$(date +%T)


function finish() {
    end_time=$(date +%T)
    output_file=$start_time-$end_time-"net.csv"
    echo -e $NET_USAGE >> $output_file

    exit 
}

trap finish SIGINT

while true ; do
        RX=`sudo cat /sys/class/net/${IF}/statistics/rx_bytes`
        TX=`sudo cat /sys/class/net/${IF}/statistics/tx_bytes`
        if [ $RXPREV -ne -1 ] ; then
                let BWRX=$RX-$RXPREV
                let BWTX=$TX-$TXPREV
                NET_USAGE+="\n$(date +%T)  $BWRX  $BWTX"
        fi
         
        RXPREV=$RX
        TXPREV=$TX
        sleep 1
done
