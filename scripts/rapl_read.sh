#!/bin/bash


# Reference: https://www.kernel.org/doc/Documentation/power/powercap/powercap.txt
# Default interface 



pkg1="/sys/class/powercap/intel-rapl/subsystem/intel-rapl:0/"
pkg2="/sys/class/powercap/intel-rapl/subsystem/intel-rapl:1/"

echo "Time  Package0  DRAM0  Package1  DRAM1 Total"

p0=$(cat $pkg1"energy_uj")
d0=$(cat $pkg1"intel-rapl:0:0/energy_uj")

p1=$(cat $pkg2"energy_uj")
d1=$(cat $pkg2"intel-rapl:1:0/energy_uj")

total=$p0+$d0+$p1+$d1

while true; do

    sleep 1

    new_p0=$(cat $pkg1"energy_uj")
    new_d0=$(cat $pkg1"intel-rapl:0:0/energy_uj")

    new_p1=$(cat $pkg2"energy_uj")
    new_d1=$(cat $pkg2"intel-rapl:1:0/energy_uj")

    let dp0=($new_p0-$p0)/1000000
    let dd0=($new_d0-$d0)/1000000
    let dp1=($new_p1-$p1)/1000000
    let dd1=($new_d1-$d1)/1000000
    let total=$dp0+$dd0+$dp1+$dd1
    
    echo "$(date +%T:%3N)  "$dp0"  "$dd0"  "$dp1"  "$dd1"  "$total

    p0=$new_p0
    d0=$new_d0
    p1=$new_p1
    d1=$new_d1
    
done
