#!/bin/bash
# Prepared by Arif Ahmed (arifch2009@gmail.com)

# Reference: https://www.kernel.org/doc/Documentation/block/stat.txt
# Default interface 
IF="sda" # change this interface name 


DISK_USAGE="Time  Read(B/s)  Write(B/s)"

start_time=$(date +%T)


function finish() {
    end_time=$(date +%T)
    output_file=$start_time-$end_time-"disk.csv"
    echo -e $DISK_USAGE >> $output_file
  
    exit 
}

trap finish SIGINT

DISK_STAT=`cat /sys/block/${IF}/stat | awk '{print $3, $7}'`

PREV_DISK_READ=`echo $DISK_STAT | awk '{print $1}'`
PREV_DISK_WRITE=`echo $DISK_STAT | awk '{print $2}'`

sleep 1

while true ; do
        DISK_STAT=`cat /sys/block/${IF}/stat | awk '{print $3, $7}'`
        CUR_DISK_READ=`echo $DISK_STAT | awk '{print $1}'`
        CUR_DISK_WRITE=`echo $DISK_STAT | awk '{print $2}'`
        let "THRD=(($CUR_DISK_READ-$PREV_DISK_READ)*512)"
        let "THWT=(($CUR_DISK_WRITE-$PREV_DISK_WRITE)*512)" 
        DISK_USAGE+="\n$(date +%T)  $THRD  $THWT"
        PREV_DISK_READ=$CUR_DISK_READ
        PREV_DISK_WRITE=$CUR_DISK_WRITE
        sleep 1
done
