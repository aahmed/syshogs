import sys, getopt, os, signal
from subprocess import Popen
from datetime import datetime
import subprocess
import time

def main(argv):

    # current time

  
    
    commands = ["./mon-cpu.sh", "./mon-mem.sh",  "./mon-net.sh", "./mon-disk.sh", "./mon-rapl.sh"]

  


    script_dir = os.path.realpath(os.path.dirname(sys.argv[0]))

    procs = [ Popen(i, cwd=script_dir+"/scripts/") for i in commands ]


    print("Monitoring CPU, Memory, network,  disk and RAPL.... \n Press (Ctrl + C) to finish the monitoring")
    for p in procs:
        p.wait()
    


def handler(signum, frame):

    now = datetime.now()
    current_time = now.strftime("%H-%M-%S")
    os.mkdir("/tmp/"+current_time) 
    output_dir = "/tmp/"+current_time

    cmd = 'mv scripts/*.csv '+output_dir

    time.sleep(3)
    
    os.system(cmd)
    print("Terminating the monitoring.")

if __name__ == "__main__":
    signal.signal(signal.SIGINT, handler)
    main(sys.argv[1:])

